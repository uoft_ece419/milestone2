package app_kvEcs;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

import client.KVStore;
import common.messages.ECSMessage.ECS_Status;
import common.messages.MetadataEntry;
import common.messages.ECSMessage;
import common.messages.KVMessage;
import common.messages.TextMessage;
import common.messages.KVMessage.StatusType;
import common.messages.TextMessage.MessageParameters;
import logger.LogSetup;

import java.io.BufferedReader;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.lang.ProcessBuilder;

import java.net.Socket;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.DigestException;
import java.util.Collections;
import java.util.Comparator;
import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;












/*
 * 
 * Used to provide an API interface to the ECSClient:
 * 
 */
public class ECSFunc {

	static Logger logger = Logger.getRootLogger();

	// Receiver buffer/message parameters:
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 1024 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;
	private OutputStream output;
	private InputStream input;

	// API specific local variables:
	private boolean running = true;	
	
	
	// Corner case handling via master server (removing the last server):
	//
	//
	private final String MASTER_SERVER_FILE = "./master_server.config";
	Boolean MASTER_SERVER_CREATED = false;
	String master_server_hostname = null;
	int master_server_port = -1;
	
	
	/**
	 * Constructor
	 * 
	 * @param master_server_read: Did the ECSClient read a master server file on startup.
	 * @param _master_server_hostname: The master server hostname.
	 * @param _master_server_port: THe master server port.
	 */
	public ECSFunc(Boolean master_server_read, String _master_server_hostname, int _master_server_port){
	
		this.MASTER_SERVER_CREATED = master_server_read;
		this.master_server_hostname = _master_server_hostname;
		this.master_server_port = _master_server_port;
	}
	
	
	
	
	
	/*
	 * Keeps track of one connection to a server (along with the consistent hashing information)
	 */
	public class ServerConnection{
		
		// Connection to one KVServer
		public Socket connection;
		public OutputStream output;
		public InputStream input;
		Boolean connection_open = true;
		
		// Hash ring entry:
		public MetadataEntry metadata = null;
		Boolean service_started = false;
		
		// Configuration info:
		public ServerEntry server_info = null;
		
		
		public ServerConnection(Socket _connection, OutputStream _output, InputStream _input, String _hostname, int _port, ServerEntry server_info){
			this.connection = _connection;
			this.output = _output;
			this.input = _input;
			this.metadata = new MetadataEntry(_hostname, _port);
			this.server_info = server_info;
		}
		
		
		
		// Starting/stopping of server storage service:
		public void set_service_status(Boolean status){
			this.service_started = status;
		}
		public Boolean get_service_status(){
			return this.service_started;
		}
		
	}
	List <ServerConnection> server_connections = new ArrayList<ServerConnection>();
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Starts "number_of_nodes" many KVServers using ssh calls, tracks them in "server_connections".
	 * 
	 * @param number_of_nodes:
	 *            Number of KVServers to start
	 * @param cache_size:
	 *            Size of the cache for each server
	 * @param replacement_strategy:
	 *            Memory cache replacement strategy for each server
	 * @param available_servers:
	 *            List of server hostnames, ports and jar path locations	  
	 *          
	 * 
	 */
	public void initService(String number_of_nodes, String cache_size, String replacement_strategy,
			List<ServerEntry> available_servers) {

	
		
		// Check if the number of nodes is reasonable:
		int num_nodes = 99999;
		try {
			num_nodes = Integer.parseInt(number_of_nodes);
		} catch (NumberFormatException e) {
			System.out.println("Failed to read number of nodes \"" + number_of_nodes + "\".");
			return;
		}
		if (available_servers.size() < num_nodes) {
			System.out.println("There are only " + available_servers.size() + " many servers available to launch.");
			return;
		}
		
		// Far simpler funciton:
		if(num_nodes == 1){
			addNode(cache_size, replacement_strategy, available_servers);
		}
		
		
		
		
		
		
		
		/*
		 * 
		 * CASE 1: 
		 * 			- We have a master server (a server that was shutoff with all the persistent data saved.)
		 * 			- No nodes will be in service:
		 * 
		 */
		if(MASTER_SERVER_CREATED){
			
			
			/* 
			 * 
			 * Get the recorded master server:
			 * 
			 * 
			 */
			logger.info("\n\n\nStarting up the master server of ip and port (" + master_server_hostname +"," + master_server_port + ").\n\n\n");

			// We are taking care of it here:
			MASTER_SERVER_CREATED = false;
			
			// Find the master server in the available servers:
			int master_index = 0;
			for(ServerEntry available_server : available_servers){
				
				// If we find the master server index:
				if(available_server.hostname.equals(master_server_hostname) && available_server.port == master_server_port)
					break;
				else
					master_index++;
			}
			
			ServerEntry server_to_add = available_servers.remove(master_index);
			ServerConnection master_server = null;
			num_nodes--;
			
			
			
			
			
			
			/* 
			 * Attempt to launch MASTER KVServer via SSH:
			 * 
			 */
			Boolean succesful = true;
			try {

				// Establish a session to connect to the SSH server (port 22)
				JSch jsch = new JSch();
				String privateKey = "~/.ssh/id_rsa";
				jsch.addIdentity(privateKey);
				Session session = jsch.getSession(null, server_to_add.hostname, 22);
				
				// Do not prompt:
				java.util.Properties config = new java.util.Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
			
				session.connect();

				
				// Command to enter folder of server jar file:
				String command1 = "cd " + server_to_add.jar_path.substring(0, server_to_add.jar_path.lastIndexOf("/"));
				
				// Command to run the server jar file:
				String command2 = "java -jar " + 
						server_to_add.jar_path.substring(server_to_add.jar_path.lastIndexOf("/")+1, server_to_add.jar_path.length())
						+ " " + Integer.toString(server_to_add.port)
						+ " " + cache_size + " " + replacement_strategy;
				
				
				logger.info("Starting server \""+ server_to_add.hostname + "\" port \"" +server_to_add.port+"\".");;
				Channel channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(command1 + "&& " + command2);

				
				
				// Read the response:
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);
				InputStream in = channel.getInputStream();
				channel.connect();

				// Output the response to the CLI:
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				String line;
				int index = 0;


				// Read up to three lines from start up of server
				while (index < 3 &&  ((line = reader.readLine()) != null)) {
					index++;
					logger.info(index + " : " + line);
				}

				channel.disconnect();
				session.disconnect();
			

			} catch (Exception e) {
				succesful = false;
				logger.error("Error: " + e);
			}
			

			
			
			
			
			

			
			
			
			
			/*
			 *  If we successfully started up the current server via SSH, we should connect:
			 *  
			 */
			Boolean successful_connect = false;
			
			if(succesful){


	 			// Try to get the connect response from the newly launched KVServer:
	 			try {

	 				
	 				// Wait 2 seconds to give time for the server to start accepting connections:
	 				//
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						logger.error(e);
					}

					
					// Attempt to connect:
					Socket new_connection = new Socket(server_to_add.hostname, server_to_add.port);

					output = new_connection.getOutputStream();
					input = new_connection.getInputStream();
					running = true;
					TextMessage connect_response = receiveMessage();

	 				
					
					
	 				// On connection success:
	 				if(connect_response != null){
	 					master_server = new ServerConnection(new_connection, output, input, server_to_add.hostname, server_to_add.port, server_to_add);
		 				server_connections.add(master_server);	
	 					logger.info(connect_response.getMsg()); // Server informs client they have established a connection
	 					successful_connect = true;
	 				}
	 				
	 			
	 				// Connection closes when attempting to read a message:
	 				else{
	 					logger.error("Connection closed during connect phase to the following server:\n");
	 					logger.error("Hostname: \"" + server_to_add.hostname + "\"\n");
	 					logger.error("Port: \"" + server_to_add.port + "\"\n");
	 					logger.error("Jar Path: \"" + server_to_add.jar_path + "\"\n");
	 				}
	 				
	 			} 
	 			
	 			// No such host exists, unlikely if we were able to connect through SSH earlier...
	 			catch (UnknownHostException e){
	 				logger.error("Hostname was not found for the following server:\n");
					logger.error("Hostname: \"" + server_to_add.hostname + "\"\n");
	 			}
	 			// Trouble opening a socket:
	 			catch (IOException ioe) {
	 				logger.error("Input and output streams could not be established for the following server:");
	 				logger.error("Hostname: \"" + server_to_add.hostname + "\"\n");
					logger.error("Port: \"" + server_to_add.port + "\"\n");
					logger.error("Jar Path: \"" + server_to_add.jar_path + "\"\n");
	 			}
	 			
			}
			
			// Add the server back to the available server list (at the end) if SSH or connect fails:
			//
			if(!successful_connect){
				available_servers.add(server_to_add);
				logger.fatal("\n\n\nWARNING, failed the starting up the master server of ip and port (" + master_server_hostname +"," + master_server_port + ").\n\n\n");
				return;
			}
			
		
			
			
			
			
			
			

			
			

			/* 
			 * 
			 * If we are here, master server was successfully started, now we start up the remaining servers:
			 * 
			 * 
			 */
			
			// While we want to randomly launch THE OTHER REMAINING KVServers via SSH:
			//
			while (num_nodes != 0) {

				num_nodes--;
				ServerEntry current_server = available_servers.remove(0);


				
				

				/* 
				 * Attempt to launch KVServer via SSH:
				 * 
				 */
				succesful = true;
				try {

					// Establish a session to connect to the SSH server (port 22)
					JSch jsch = new JSch();
					String privateKey = "~/.ssh/id_rsa";
					jsch.addIdentity(privateKey);
					Session session = jsch.getSession(null, current_server.hostname, 22);
					
					// Do not prompt:
					java.util.Properties config = new java.util.Properties();
					config.put("StrictHostKeyChecking", "no");
					session.setConfig(config);
				
					session.connect();

					
					// Command to enter folder of server jar file:
					String command1 = "cd " + current_server.jar_path.substring(0, current_server.jar_path.lastIndexOf("/"));
					
					// Command to run the server jar file:
					String command2 = "java -jar " + 
							current_server.jar_path.substring(current_server.jar_path.lastIndexOf("/")+1, current_server.jar_path.length())
							+ " " + Integer.toString(current_server.port)
							+ " " + cache_size + " " + replacement_strategy;
					
					
					logger.info("Starting server \""+ current_server.hostname + "\" port \"" +current_server.port+"\".");;
					Channel channel = session.openChannel("exec");
					((ChannelExec) channel).setCommand(command1 + "&& " + command2);

					
					
					// Read the response:
					channel.setInputStream(null);
					((ChannelExec) channel).setErrStream(System.err);
					InputStream in = channel.getInputStream();
					channel.connect();

					// Output the response to the CLI:
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					String line;
					int index = 0;


					// Read up to three lines from start up of server
					while (index < 3 &&  ((line = reader.readLine()) != null)) {
						index++;
						logger.info(index + " : " + line);
					}

					channel.disconnect();
					session.disconnect();
				

				} catch (Exception e) {
					succesful = false;
					logger.error("Error: " + e);
				}
				

				
				
				
				
				

				
				
				
				
				/*
				 *  If we successfully started up the current server via SSH, we should connect:
				 *  
				 */
				successful_connect = false;
				
				if(succesful){


		 			// Try to get the connect response from the newly launched KVServer:
		 			try {

		 				
		 				// Wait 2 seconds to give time for the server to start accepting connections:
		 				//
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							logger.error(e);
						}

						
						// Attempt to connect:
						Socket new_connection = new Socket(current_server.hostname, current_server.port);

						output = new_connection.getOutputStream();
						input = new_connection.getInputStream();
						running = true;
						TextMessage connect_response = receiveMessage();

		 				
						
						
		 				// On connection success:
		 				if(connect_response != null){
			 				server_connections.add(new ServerConnection(new_connection, output, input, current_server.hostname, current_server.port, current_server));	
		 					logger.info(connect_response.getMsg()); // Server informs client they have established a connection
		 					successful_connect = true;
		 				}
		 				
		 			
		 				// Connection closes when attempting to read a message:
		 				else{
		 					logger.error("Connection closed during connect phase to the following server:\n");
		 					logger.error("Hostname: \"" + current_server.hostname + "\"\n");
		 					logger.error("Port: \"" + current_server.port + "\"\n");
		 					logger.error("Jar Path: \"" + current_server.jar_path + "\"\n");
		 				}
		 				
		 			} 
		 			
		 			// No such host exists, unlikely if we were able to connect through SSH earlier...
		 			catch (UnknownHostException e){
		 				logger.error("Hostname was not found for the following server:\n");
						logger.error("Hostname: \"" + current_server.hostname + "\"\n");
		 			}
		 			// Trouble opening a socket:
		 			catch (IOException ioe) {
		 				logger.error("Input and output streams could not be established for the following server:");
		 				logger.error("Hostname: \"" + current_server.hostname + "\"\n");
						logger.error("Port: \"" + current_server.port + "\"\n");
						logger.error("Jar Path: \"" + current_server.jar_path + "\"\n");
		 			}
		 			
				}
				
				// Add the server back to the available server list (at the end) if SSH or connect fails:
				//
				if(!successful_connect){
					available_servers.add(current_server);
				}
				

			}
			
			
		
			
			
			
			

			
			
			/*
			 * After launching all servers and connecting, we must create the consistent hashing metadata:
			 * 
			 */
			MessageDigest md = null;
			try{
				md = MessageDigest.getInstance("MD5");
			}
			catch(NoSuchAlgorithmException e){
				System.out.println("The MD5 algorithm does not exist...");
				System.exit(1);
			}

			// For each server, generate a hash:
			for (ServerConnection connection : server_connections) {

				 // Convert this into MD5 hash:
				 String temp = connection.metadata.hostname + Integer.toString(connection.metadata.port);
				 byte[] to_hash = temp.getBytes();
				 
				 // Generate hash:
			     md.update(to_hash);
			     byte[] new_hash = md.digest();
			     connection.metadata.hash = new_hash;
			}
			
			
			
			
			// Sort the server connections based on hash values:
			//
			Collections.sort(server_connections, new Comparator<ServerConnection>() {
				@Override
				public int compare(ServerConnection s1, ServerConnection s2) {
					
					/*
					 * Determines which hash comes first
					 */
					for (int i = 0, j = 0; i < s1.metadata.hash.length && j < s2.metadata.hash.length; i++, j++) {
						int a = (s1.metadata.hash[i] & 0xff);
						int b = (s2.metadata.hash[j] & 0xff);
						if (a != b) {
							return a - b;
						}
					}
					return s1.metadata.hash.length - s2.metadata.hash.length;
				}
			});
			
			// Previous hash range is predecessor node:
			initialize_servers_metadata();
			
			
			// Print hash ranges for each server successfully launched and connected to:
			int server_num = 1;
			logger.info("\n\nConsistent Hashing: Server Hash Ranges:\n");
			for (ServerConnection connection : server_connections) {
				logger.info("Server "+server_num+":("+ connection.metadata.hostname + ","+ connection.metadata.port + ")");
				logger.info("Start Range   ----     End Range" );
				logger.info(DatatypeConverter.printHexBinary(connection.metadata.prev_hash_range) + "------" + DatatypeConverter.printHexBinary(connection.metadata.hash));
				logger.info("=====");
				server_num++;
			}
			logger.info("\n\n");
			
			
			
			
			
			
			
			/*
			 *
			 *
			 * Now we pack the metadata into a list and send it to each server:
			 * 
			 */
			
			// Packing of metadata
			List<MetadataEntry> metadata_entries = new ArrayList<MetadataEntry>();
			for (ServerConnection connection : server_connections) {
				metadata_entries.add(connection.metadata);
			}
			// Send encapsulated string as TextMessage:
			for (ServerConnection cur_server : server_connections) {
			
				// Create message to send:
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1);
				ECS_Status response_type = forwarding_message_to_server(cur_server, encapsulated_message);
					
				
				// Check response type:
				if(response_type != null){
					
					if(response_type == ECS_Status.INITIALIZE_ACK)
						logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
					
				}
		
			}
			
			
			
			
			
			/*
			 * 
			 * 
			 * The final step: Move all data the master server is no longer responsible to other servers.
			 * 
			 * 
			 * 
			 */
			
			// Write lock the master server:
			//
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, master_server.metadata.hostname, null, null, null, -1);
	        ECS_Status response_type = forwarding_message_to_server(master_server, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received a write lock ACK from ("+ master_server.metadata.hostname +","+ master_server.metadata.port +").");
			
			
			// For each server, other than the master server itself:
			for (ServerConnection cur_server : server_connections) {
				
				// If not the port:
				if(!(cur_server.metadata.hostname.equals(master_server_hostname) && cur_server.metadata.port == master_server_port)){
					
					// Send data other server is responsible for from the master server:
					//
					encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, master_server.metadata.hostname, cur_server.metadata.hostname, cur_server.metadata.prev_hash_range, cur_server.metadata.hash, cur_server.metadata.port);
			        response_type = forwarding_message_to_server(master_server, encapsulated_message);
					
					// Check response type:
					if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
							logger.info("Received a movedata failure ACK from ("+ master_server.metadata.hostname +","+ master_server.metadata.port +").");
					else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
							logger.info("Received a movedata success ACK from ("+ master_server.metadata.hostname +","+ master_server.metadata.port +").");
				}

			}
			
			// Unlock the master server:
			encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, master_server.metadata.hostname, null, null, null, -1);
	        response_type = forwarding_message_to_server(master_server, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received a write unlock ACK from ("+ master_server.metadata.hostname +","+ master_server.metadata.port +").");
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * 
		 * CASE 2: - There is no master server.
		 * 		   - There are no nodes in service.
		 * 
		 */
		else if(!MASTER_SERVER_CREATED && (server_connections.size() == 0)){
			
			// While we want to randomly launch KVServers via SSH:
			//
			while (num_nodes != 0) {

				num_nodes--;
				ServerEntry current_server = available_servers.remove(0);


				
				

				/* 
				 * Attempt to launch KVServer via SSH:
				 * 
				 */
				Boolean succesful = true;
				try {

					// Establish a session to connect to the SSH server (port 22)
					JSch jsch = new JSch();
					String privateKey = "~/.ssh/id_rsa";
					jsch.addIdentity(privateKey);
					Session session = jsch.getSession(null, current_server.hostname, 22);
					
					// Do not prompt:
					java.util.Properties config = new java.util.Properties();
					config.put("StrictHostKeyChecking", "no");
					session.setConfig(config);
				
					session.connect();

					
					// Command to enter folder of server jar file:
					String command1 = "cd " + current_server.jar_path.substring(0, current_server.jar_path.lastIndexOf("/"));
					
					// Command to run the server jar file:
					String command2 = "java -jar " + 
							current_server.jar_path.substring(current_server.jar_path.lastIndexOf("/")+1, current_server.jar_path.length())
							+ " " + Integer.toString(current_server.port)
							+ " " + cache_size + " " + replacement_strategy;
					
					
					logger.info("Starting server \""+ current_server.hostname + "\" port \"" +current_server.port+"\".");;
					Channel channel = session.openChannel("exec");
					((ChannelExec) channel).setCommand(command1 + "&& " + command2);

					
					
					// Read the response:
					channel.setInputStream(null);
					((ChannelExec) channel).setErrStream(System.err);
					InputStream in = channel.getInputStream();
					channel.connect();

					// Output the response to the CLI:
					BufferedReader reader = new BufferedReader(new InputStreamReader(in));
					String line;
					int index = 0;


					// Read up to three lines from start up of server
					while (index < 3 &&  ((line = reader.readLine()) != null)) {
						index++;
						logger.info(index + " : " + line);
					}

					channel.disconnect();
					session.disconnect();
				

				} catch (Exception e) {
					succesful = false;
					logger.error("Error: " + e);
				}
				

				
				
				
				
				

				
				
				
				
				/*
				 *  If we successfully started up the current server via SSH, we should connect:
				 *  
				 */
				Boolean successful_connect = false;
				
				if(succesful){


		 			// Try to get the connect response from the newly launched KVServer:
		 			try {

		 				
		 				// Wait 2 seconds to give time for the server to start accepting connections:
		 				//
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							logger.error(e);
						}

						
						// Attempt to connect:
						Socket new_connection = new Socket(current_server.hostname, current_server.port);

						output = new_connection.getOutputStream();
						input = new_connection.getInputStream();
						running = true;
						TextMessage connect_response = receiveMessage();

		 				
						
						
		 				// On connection success:
		 				if(connect_response != null){
			 				server_connections.add(new ServerConnection(new_connection, output, input, current_server.hostname, current_server.port, current_server));	
		 					logger.info(connect_response.getMsg()); // Server informs client they have established a connection
		 					successful_connect = true;
		 				}
		 				
		 			
		 				// Connection closes when attempting to read a message:
		 				else{
		 					logger.error("Connection closed during connect phase to the following server:\n");
		 					logger.error("Hostname: \"" + current_server.hostname + "\"\n");
		 					logger.error("Port: \"" + current_server.port + "\"\n");
		 					logger.error("Jar Path: \"" + current_server.jar_path + "\"\n");
		 				}
		 				
		 			} 
		 			
		 			// No such host exists, unlikely if we were able to connect through SSH earlier...
		 			catch (UnknownHostException e){
		 				logger.error("Hostname was not found for the following server:\n");
						logger.error("Hostname: \"" + current_server.hostname + "\"\n");
		 			}
		 			// Trouble opening a socket:
		 			catch (IOException ioe) {
		 				logger.error("Input and output streams could not be established for the following server:");
		 				logger.error("Hostname: \"" + current_server.hostname + "\"\n");
						logger.error("Port: \"" + current_server.port + "\"\n");
						logger.error("Jar Path: \"" + current_server.jar_path + "\"\n");
		 			}
		 			
				}
				
				// Add the server back to the available server list (at the end) if SSH or connect fails:
				//
				if(!successful_connect){
					available_servers.add(current_server);
				}
				

			}
			
			
		
			
			
			
			

			
			
			/*
			 * After launching all servers and connecting, we must create the consistent hashing metadata:
			 * 
			 */
			MessageDigest md = null;
			try{
				md = MessageDigest.getInstance("MD5");
			}
			catch(NoSuchAlgorithmException e){
				System.out.println("The MD5 algorithm does not exist...");
				System.exit(1);
			}


			// For each server, generate a hash:
			for (ServerConnection connection : server_connections) {

				 // Convert this into MD5 hash:
				 String temp = connection.metadata.hostname + Integer.toString(connection.metadata.port);
				 byte[] to_hash = temp.getBytes();
				 
				 // Generate hash:
			     md.update(to_hash);
			     byte[] new_hash = md.digest();
			     connection.metadata.hash = new_hash;
			}
			
			
			// Sort the server connections based on hash values:
			//
			Collections.sort(server_connections, new Comparator<ServerConnection>() {
				@Override
				public int compare(ServerConnection s1, ServerConnection s2) {
					
					/*
					 * Determines which hash comes first
					 */
					for (int i = 0, j = 0; i < s1.metadata.hash.length && j < s2.metadata.hash.length; i++, j++) {
						int a = (s1.metadata.hash[i] & 0xff);
						int b = (s2.metadata.hash[j] & 0xff);
						if (a != b) {
							return a - b;
						}
					}
					return s1.metadata.hash.length - s2.metadata.hash.length;
				}
			});
			
			// Previous hash range is predecessor node:
			initialize_servers_metadata();
			
			
			// Print hash ranges for each server successfully launched and connected to:
			int server_num = 1;
			logger.info("\n\nConsistent Hashing: Server Hash Ranges:\n");
			for (ServerConnection connection : server_connections) {
				logger.info("Server "+server_num+":("+ connection.metadata.hostname + ","+ connection.metadata.port + ")");
				logger.info("Start Range   ----     End Range" );
				logger.info(DatatypeConverter.printHexBinary(connection.metadata.prev_hash_range) + "------" + DatatypeConverter.printHexBinary(connection.metadata.hash));
				logger.info("=====");
				server_num++;
			}
			logger.info("\n\n");
			
			
			
			
			
			
			
			/*
			 * Now we pack the metadata into a list and send it to each server:
			 * 
			 */
			
			
			// Packing of metadata
			List<MetadataEntry> metadata_entries = new ArrayList<MetadataEntry>();
			for (ServerConnection connection : server_connections) {
				metadata_entries.add(connection.metadata);
			}
			
			
		
			
			// Send encapsulated string as TextMessage:
			for (ServerConnection cur_server : server_connections) {
			
				// Create message to send:
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1);
				ECS_Status response_type = forwarding_message_to_server(cur_server, encapsulated_message);
					
				
				// Check response type:
				if(response_type != null){
					
					if(response_type == ECS_Status.INITIALIZE_ACK)
						logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
					
				}
		
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * 
		 * CASE 3: - There is no master server.
		 * 		   - There are already servers in service.
		 * 
		 * NOTE: This can be done more efficiently but it is a HUGE pain to make efficient.
		 * 
		 */
		else{
			
			// While we want to randomly launch KVServers via SSH:
			//
			while (num_nodes != 0) {
				num_nodes--;
				addNode(cache_size, replacement_strategy, available_servers);
			}
				
		}
	
		
		
	}

	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param cache_size:
	 *            Size of the cache for each server
	 * @param replacement_strategy:
	 *            Memory cache replacement strategy for each server
	 * @param available_servers:
	 *            List of server hostnames, ports and jar path locations	
	 * @return success:
	 *			  0 if success, -1 if failure
	 *          
	 * 
	 */
	public int addNode(String cache_size, String replacement_strategy,
			List<ServerEntry> available_servers){
		
		//If there are idle servers in the repository,
		//randomly pick one of them and send an SSH call to invoke the KVServer process.
		ServerEntry server_to_add = null;
		
		
		// If one server contains all relevant data:
		if(MASTER_SERVER_CREATED){
		
			logger.info("\n\n\nStarting up the master server of ip and port (" + master_server_hostname +"," + master_server_port + ").\n\n\n");

			// We are taking care of it here:
			MASTER_SERVER_CREATED = false;
			
			// Find the master server in the available servers:
			int master_index = 0;
			for(ServerEntry available_server : available_servers){
				
				// If we find the master server index:
				if(available_server.hostname.equals(master_server_hostname) && available_server.port == master_server_port)
					break;
				else
					master_index++;
			}
			
			server_to_add = available_servers.remove(master_index);
		}
		
		
		else{
			
			try{
				//todo: need to RANDOMLY PICK, but right now for testing, just remove the first one
				server_to_add = available_servers.remove(0);
			}catch(NoSuchElementException e){
				System.out.println("There are no more available servers to launch.");
				return -1;
			}
		
		}
		
		
		
		/*
		 * Start a KVServer via SSH calls:
		 * 
		 */
		Boolean succesful = true;
		try {

			// Establish a session to connect to the SSH server (port 22)
			JSch jsch = new JSch();
			String privateKey = "~/.ssh/id_rsa";
			jsch.addIdentity(privateKey);
			Session session = jsch.getSession(null, server_to_add.hostname, 22);
			
			// Do not prompt:
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
		
			session.connect();

			
			// Command to enter folder of server jar file:
			String command1 = "cd " + server_to_add.jar_path.substring(0, server_to_add.jar_path.lastIndexOf("/"));
			
			// Command to run the server jar file:
			String command2 = "java -jar " + 
					server_to_add.jar_path.substring(server_to_add.jar_path.lastIndexOf("/")+1, server_to_add.jar_path.length())
					+ " " + Integer.toString(server_to_add.port)
					+ " " + cache_size + " " + replacement_strategy;
			
			
			logger.info("Starting server \""+ server_to_add.hostname + "\" port \"" +server_to_add.port+"\".");;
			Channel channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command1 + "&& " + command2);

			
			
			// Read the response:
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
			InputStream in = channel.getInputStream();
			channel.connect();

			// Output the response to the CLI:
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			int index = 0;


			// Read up to three lines from start up of server
			while (index < 3 &&  ((line = reader.readLine()) != null)) {
				index++;
				logger.info(index + " : " + line);
			}

			channel.disconnect();
			session.disconnect();
		

		} catch (Exception e) {
			succesful = false;
			logger.error("Error: " + e);
		}
		
		
		
		
		
		
		
		
		/*
		 *  If we successfully started up the current server via SSH, we should connect:
		 *  
		 */
		if(succesful){


			// Try to get the connect response from the newly launched KVServer:
			try {

				
				// Wait 2 seconds to give time for the server to start accepting connections:
				//
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					logger.error(e);
				}

				
				// Attempt to connect:
				Socket new_connection = new Socket(server_to_add.hostname, server_to_add.port);

				output = new_connection.getOutputStream();
				input = new_connection.getInputStream();
				running = true;
				TextMessage connect_response = receiveMessage();

				
				
				
				// On connection success:
				if(connect_response != null){
					server_connections.add(new ServerConnection(new_connection, output, input, server_to_add.hostname, server_to_add.port, server_to_add));	
					logger.info(connect_response.getMsg()); // Server informs client they have established a connection
				}
				
			
				// Connection closes when attempting to read a message:
				else{
					logger.error("Connection closed during connect phase to the following server:\n");
					logger.error("server_to_add.hostname: \"" + server_to_add.hostname + "\"\n");
					logger.error("Port: \"" + server_to_add.port + "\"\n");
					logger.error("Jar Path: \"" + server_to_add.jar_path + "\"\n");
				}
				
			} 
			
			// No such host exists, unlikely if we were able to connect through SSH earlier...
			catch (UnknownHostException e){
				logger.error("Hostname was not found for the following server:\n");
				logger.error("Hostname: \"" + server_to_add.hostname + "\"\n");
			}
			// Trouble opening a socket:
			catch (IOException ioe) {
				logger.error("Input and output streams could not be established for the following server:");
				logger.error("Hostname: \"" + server_to_add.hostname + "\"\n");
				logger.error("Port: \"" + server_to_add.port + "\"\n");
				logger.error("Jar Path: \"" + server_to_add.jar_path + "\"\n");
			}
			
		}else{
			//if the launch is not successful, then put the server back in the deque
			try{
				available_servers.add(server_to_add);
			}catch(Exception e){
				logger.error("cannot put unsuccessfully launched server back in the deque");
			}
			return -1;
		}
		
		
		
		
		
		
		//Determine the position of the new storage server within the ring by hashing its address.
		//update the metadata of the storage service
		MessageDigest md = null;
		try{
			md = MessageDigest.getInstance("MD5");
		}
		catch(NoSuchAlgorithmException e){
			System.out.println("The MD5 algorithm does not exist...");
			System.exit(1);
		}


		 // For each server, generate a hash:
		 // Convert this into MD5 hash:
		 ServerConnection connection = server_connections.get(server_connections.size()-1);//get the recently added connection
		 String temp = connection.metadata.hostname + Integer.toString(connection.metadata.port);
		 byte[] to_hash = temp.getBytes();
		 
		 // Generate hash:
		 md.update(to_hash);
		 byte[] new_hash = md.digest();
		 connection.metadata.hash = new_hash;
		
		
		
		// Sort the server connections based on hash values:
		//
		Collections.sort(server_connections, new Comparator<ServerConnection>() {
			@Override
			public int compare(ServerConnection s1, ServerConnection s2) {
				
				/*
				 * Determines which hash comes first
				 */
				for (int i = 0, j = 0; i < s1.metadata.hash.length && j < s2.metadata.hash.length; i++, j++) {
					int a = (s1.metadata.hash[i] & 0xff);
					int b = (s2.metadata.hash[j] & 0xff);
					if (a != b) {
						return a - b;
					}
				}
				return s1.metadata.hash.length - s2.metadata.hash.length;
			}
		});
		// Go around the ring and set the hash ranges:
		initialize_servers_metadata();
		print_all_hash_info();
		
		
		
		
		// Packing of metadata and ONLY send to the newly initialized server first
		List<MetadataEntry> metadata_entries = new ArrayList<MetadataEntry>();
		for (ServerConnection i_connection : server_connections) {
			metadata_entries.add(i_connection.metadata);
		}
		// Create message to send:
		ECSMessage encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, server_to_add.hostname, null, null, null, -1);
		ECS_Status response_type = forwarding_message_to_server(connection, encapsulated_message);
		
		// Check response type:
		if(response_type != null && response_type == ECS_Status.INITIALIZE_ACK)
				logger.info("Received initialization of metadata response from ("+ server_to_add.hostname +","+ server_to_add.port +").");
		
		
		
		
		
		// If there is only one server then just initialize it and return
		// to avoid locking itself. The server would be it's own successor.
		if(server_connections.size()==1){
			return 0;
		}
		// Find the successor
		ServerConnection successor_server = get_successor(connection);
		if(successor_server == null){
			logger.error("cannot get successor server");
			return -1;
		}

		// Set write lock on the successor
		encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, server_to_add.hostname, null, null, null, -1);
        response_type = forwarding_message_to_server(successor_server, encapsulated_message);
		
		// Check response type:
		if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
			logger.info("Received write lock ACK from ("+ successor_server.metadata.hostname +","+ successor_server.metadata.port +").");
	
		
		
		
		
		
		
		// Invoke the transfer of the affected data items to the new storage server.
		// The data that is transferred should not be deleted immediately to be able 
		// to serve read requests in the meantime at the successor while transfer is in progress
		//
		//	NOTE: We send the previous of the current server's hash as the start, and the current server's hash as the end
		//
		encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, server_to_add.hostname, connection.metadata.hostname, connection.metadata.prev_hash_range, connection.metadata.hash, connection.metadata.port);
        response_type = forwarding_message_to_server(successor_server, encapsulated_message);
		
		// Check response type:
		if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
				logger.info("Received a movedata failure ACK from ("+ successor_server.metadata.hostname +","+ successor_server.metadata.port +").");
		else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
				logger.info("Received a movedata success ACK from ("+ successor_server.metadata.hostname +","+ successor_server.metadata.port +").");

		
		
		
		
		
		
		// Send a metadata update to all storage servers(the rest of the servers other than the newly instantiated one)
		//
		for (ServerConnection cur_server : server_connections) {
			
			// Create message to send:
			encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1);
			response_type = forwarding_message_to_server(cur_server, encapsulated_message);
				
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.INITIALIZE_ACK)
				logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");		
		}
	
		
		
		
		
		
		// Release the write lock on the successor server 
		// and finally remove the data items that are no longer handled by this server
		//
		encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, server_to_add.hostname, null, null, null, -1);
        response_type = forwarding_message_to_server(successor_server, encapsulated_message);
		
		// Check response type:
		if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
			logger.info("Received write unlock ACK from ("+ successor_server.metadata.hostname +","+ successor_server.metadata.port +").");
		
		return 0;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Attempts to remove a KVServer node.
	 * 
	 * @param available_servers: The pool of available servers (to add the server back).
	 * @return: 0  - Success
	 * 			-1 - Failure
	 */
	public int removeNode(List<ServerEntry> available_servers){
		
		
		Boolean is_removing_last_server = false;
		
		// Check if server_connections is empty
		if(server_connections.isEmpty()){
			logger.error("Cannot remove a node because no server is connected to the ECS.");
			return -1;
		}
		
		// If there is only one server, then there won't be any successor. 
		if(server_connections.size()==1){
			is_removing_last_server = true;
		}
		
		
		
		// One sever remaining:
		if(is_removing_last_server){
			
			ServerConnection sc_to_remove = server_connections.remove(0);
			
			// Stop the service of the server:
			// Create message to send:
			// NOTE: We do not delete entries of the last server!
			//
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.SHUTDOWN_SOFT, null, sc_to_remove.metadata.hostname, null, null, null, -1);
			ECS_Status response_type = forwarding_message_to_server(sc_to_remove, encapsulated_message);
				
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.SHUTDOWN_ACK)
				logger.info("Received shutdown response from ("+ sc_to_remove.metadata.hostname +","+ sc_to_remove.metadata.port +").");
			
				
			// Add the server back to the available list of servers:
			available_servers.add(sc_to_remove.server_info);
			MASTER_SERVER_CREATED = true;
			master_server_hostname = sc_to_remove.metadata.hostname;
			master_server_port = sc_to_remove.metadata.port;
			update_master_file();
			logger.info("\n\n\nSelected master server of ip and port (" + master_server_hostname +"," + master_server_port + ").\n\n\n");
			
			return 0;
			
		}
		
		// Multiple servers:
		else{
			
			
			// MISSING RANDOM INDEX:
			//
			int remove_index = 0;
			ServerConnection sc_to_remove = server_connections.get(remove_index);
			ServerConnection sc_successor = get_successor(sc_to_remove);
			if(sc_successor == null){
				logger.error("cannot get successor server");
				return -1;
			}
			sc_to_remove = server_connections.remove(remove_index);

			
			// Recalculate and update the metadata of the storage serviceRecalculate and update the metadata of the storage service
			// (Note: Removing an element from a sorted list ---> still sorted)
			//
			initialize_servers_metadata();
			print_all_hash_info();
			
			
			
			// Set the write_lock() on the storage server that has to be deleted.
			//
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, sc_to_remove.metadata.hostname, null, null, null, -1);
			ECS_Status response = forwarding_message_to_server(sc_to_remove, encapsulated_message);
			
			if(response != null && response == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ sc_to_remove.metadata.hostname +","+ sc_to_remove.metadata.port +").");

			
			
			
			
			
			// Invoke the transfer of ALL items in the removed node to the successor:
			//
			encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, sc_successor.metadata.hostname, sc_to_remove.metadata.prev_hash_range, sc_to_remove.metadata.hash, sc_successor.metadata.port);
			response = forwarding_message_to_server(sc_to_remove, encapsulated_message);
			
			if(response != null && response == ECS_Status.MOVE_DATA_FAILURE)
				logger.info("Received a movedata failure ACK from ("+ sc_to_remove.metadata.hostname +","+ sc_to_remove.metadata.port +").");
			else if(response != null && response == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ sc_to_remove.metadata.hostname +","+ sc_to_remove.metadata.port +").");

			
			
			
			// Send a metadata update to the remaining storage servers
			// Note: First we pack the list of metadata to send
			//
			List<MetadataEntry> metadata_entries = new ArrayList<MetadataEntry>();
			for (ServerConnection i_connection : server_connections) {
				metadata_entries.add(i_connection.metadata);
			}
			for (ServerConnection cur_server : server_connections) {
				
				// Create message to send:
				encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1);
				response = forwarding_message_to_server(cur_server, encapsulated_message);
					
				
				// Check response type:
				if(response != null && response == ECS_Status.INITIALIZE_ACK)
					logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");		
			}
			
			
			
			
			// Shutdown the removed storage server (and remove all entries that are no longer bound to the removed node):
			//
			// Create a message to encapsulate and send it to a KVServer:
			encapsulated_message = new ECSMessage(ECS_Status.SHUTDOWN_HARD, null, sc_to_remove.metadata.hostname, null, null, null, -1);
			response = forwarding_message_to_server(sc_to_remove, encapsulated_message);
				
			
			// Check response type:
			if(response != null && response == ECS_Status.SHUTDOWN_ACK)
				logger.info("Received a HARD shutdown response from ("+ sc_to_remove.metadata.hostname +","+ sc_to_remove.metadata.port +").");
			
		}
		
		return 0;
		
	}

	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Sends a shutdown server message to every server and waits for a response
	 * 
	 * @return: Number of servers shutdown
	 */
	public int shutdown_all_servers(List<ServerEntry> available_servers){
		
		// Track how many servers are effected:
		int servers_shutdown = 0;
		if(server_connections.size() == 0)
			return servers_shutdown;
		
		// Can use a simpler function:
		if(server_connections.size() == 1){
			int success = removeNode(available_servers);
			
			// IF we are successful ---> one server affected.
			if(success == 0)
				return 1;
			else
				return 0;
		}
			
			
		
		
		// Set a master server that will hold on to all data:
		//
		MASTER_SERVER_CREATED = true;
		ServerConnection master_server = server_connections.remove(0);
		master_server_hostname = master_server.metadata.hostname;
		master_server_port = master_server.metadata.port;
		update_master_file();
		logger.info("\n\n\nSelected master server of ip and port (" + master_server_hostname +"," + master_server_port + ").\n\n\n");
		
		
		
		
		// For each server (other than the master): 
		// Send a write lock message, movedata to the master as well:
		//
		for (ServerConnection current_server : server_connections) {

			
			/*
			 *  (WRITE LOCK):
			 */
			// Create a message to encapsulate and send it to a KVServer:
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, current_server.metadata.hostname, null, null, null, -1);
			ECS_Status response_type = forwarding_message_to_server(current_server, encapsulated_message);
				
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK){
				logger.info("Received shutdown response from ("+ current_server.metadata.hostname +","+ current_server.metadata.port +").");
			}
			
			
			/*
			 * (MOVE DATA)
			 */
			encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, master_server.metadata.hostname, current_server.metadata.prev_hash_range, current_server.metadata.hash, master_server.metadata.port);
			response_type = forwarding_message_to_server(current_server, encapsulated_message);
			
			if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
				logger.info("Received a movedata failure ACK from ("+ current_server.metadata.hostname +","+ current_server.metadata.port +").");
			else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ current_server.metadata.hostname +","+ current_server.metadata.port +").");


			/*
			 * (HARD SHUTDOWN)
			 */
			// Shutdown the removed storage server (and remove all entries that are no longer bound to the removed node):
			//
			// Create a message to encapsulate and send it to a KVServer:
			encapsulated_message = new ECSMessage(ECS_Status.SHUTDOWN_HARD, null, current_server.metadata.hostname, null, null, null, -1);
			response_type = forwarding_message_to_server(current_server, encapsulated_message);
				
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.SHUTDOWN_ACK){
				logger.info("Received a HARD shutdown response from ("+ current_server.metadata.hostname +","+ current_server.metadata.port +").");
				servers_shutdown++;
			}

			
			/*
			 * (Add server to available pool)
			 */
			available_servers.add(current_server.server_info);
		}
		server_connections.clear(); // Remove all servers
		
		

		
		
		
		
		
		/*
		 * Now we SOFT shutdown the master server (it has all the persistent data):
		 */
		// Shutdown the removed storage server (and KEEP ALL PERSISTENT ENTRIES):
		//
		// Create a message to encapsulate and send it to a KVServer:
		ECSMessage encapsulated_message = new ECSMessage(ECS_Status.SHUTDOWN_SOFT, null, master_server.metadata.hostname, null, null, null, -1);
		ECS_Status response_type = forwarding_message_to_server(master_server, encapsulated_message);
			
		
		// Check response type:
		if(response_type != null && response_type == ECS_Status.SHUTDOWN_ACK)
			logger.info("Received a HARD shutdown response from ("+ master_server.metadata.hostname +","+ master_server.metadata.port +").");
		servers_shutdown++;
		
		// Add back to available pool:
		available_servers.add(master_server.server_info);

		return servers_shutdown;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Starts or stops storage service for all launched servers.
	 * 
	 * @param turn_service_on: Start or stop all launched servers (if they aren't already started or stopped)
	 * 
	 * @return: Number of servers affected
	 */
	public int set_service_status(Boolean turn_service_on){
		
		int servers_affected = 0;
		

		// For each server: 
		for (ServerConnection cur_server: server_connections) {

			
			// IF the service has already started on this server, skip the server:
			if(cur_server.get_service_status() && turn_service_on)
				continue;
			
			// If we want to stop the server and its already stopped, skip:
			if(!cur_server.get_service_status() && !turn_service_on)
				continue;
			
			
			
			

			
			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage encapsulated_message = null;
					
			if(turn_service_on)
				encapsulated_message = new ECSMessage(ECS_Status.START_SERVICE, null, cur_server.metadata.hostname, null, null, null, -1);
			else
				encapsulated_message = new ECSMessage(ECS_Status.STOP_SERVICE, null, cur_server.metadata.hostname, null, null, null, -1);

			// Send the actual message:
			ECS_Status response_type = forwarding_message_to_server(cur_server, encapsulated_message);

			
			
							
				
			// Take encapsulated ECS message and convert into a usable format:
			if(response_type != null){
				
				// We expect a cetain ack based on whether we are stopping or starting service:
				if(turn_service_on && response_type == ECS_Status.START_SERVICE_ACK){
					servers_affected++;
					cur_server.set_service_status(true);
					logger.info("Received a start sevice response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
				}
				else if(!turn_service_on && response_type == ECS_Status.STOP_SERVICE_ACK){
					servers_affected++;
					cur_server.set_service_status(false);
					logger.info("Received a stop sevice response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
				}
				
			}
			
		}
		
		return servers_affected;
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************************************************************************************************************
	 * 
	 * 
	 * 
	 *
	 *	 MISCELLANEOUS HELPER FUNCTIONS
	 * 
	 * 
	 * 
	 */

	/**
	 * 
	 * Updates the master file with the new master server.
	 * 
	 */
	public void update_master_file(){
		
		try{
			PrintWriter writer = new PrintWriter(MASTER_SERVER_FILE);
			writer.print(master_server_hostname + " " + master_server_port);
			writer.close();
		}
		catch(FileNotFoundException e){
			logger.error(e);
		}
	}
	
	/**
	 * helper function: update metadata field of the server_connections
	 * 
	 */
	public void initialize_servers_metadata(){
		
		//Initialize the new storage server with the updated metadata and start it.
		//only the new connection and its predecessor and successor should change the range
		int current_server_index = 0;
		for (ServerConnection i_connection : server_connections) {
			// Look at next server in the ring:
			if(current_server_index!=0){
				i_connection.metadata.prev_hash_range = server_connections.get(current_server_index-1).metadata.hash;
			}    
			current_server_index++;
		}
		// IF we are looking at the first server, the previous hash should come from the last connection
		server_connections.get(0).metadata.prev_hash_range = server_connections.get(server_connections.size()-1).metadata.hash; // First server in ring

	}
	
	/**
	 * helper function: print all the beginning hash info's of the server_connections
	 * 
	 */
	public void print_all_hash_info(){
		
		// Print hash ranges for each server successfully launched and connected to:
		int server_num = 1;
		logger.info("\n\nConsistent Hashing: Server Hash Ranges:\n");
		for (ServerConnection i_connection : server_connections) {
			logger.info("Server "+server_num+":("+ i_connection.metadata.hostname + ","+ i_connection.metadata.port + ")");
			logger.info("Start Range   ----     End Range" );
			logger.info(DatatypeConverter.printHexBinary(i_connection.metadata.prev_hash_range) + "------" + DatatypeConverter.printHexBinary(i_connection.metadata.hash));
			logger.info("=====");
			server_num++;
		}
		logger.info("\n\n");
		
	}
	
	/**
	 * helper function: find the server's successor
	 * 
	 */
	 ServerConnection get_successor(ServerConnection connection){
		 
		int cur_server_index = get_index_from_server_connections(connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server");
			return null;
		}
		
		ServerConnection successor_server = null;
		
		if(cur_server_index == server_connections.size()-1){//last server
			successor_server = server_connections.get(0);
		}else{
			successor_server = server_connections.get(cur_server_index+1);
		}
		
		return successor_server;
		 
	 }
	
	/**
	 * helper function: find the index of the server in server_connections
	 * 
	 */
	int get_index_from_server_connections(ServerConnection connection){
		
		int connection_add_index = server_connections.lastIndexOf(connection);
		if(connection_add_index == -1){
			logger.error("cannot find the connection just added. This shouldn't happen");
			return -1;
		}else{
			return connection_add_index;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Encapsulates an ECSMessage into a TextMessage and sends it to a given ServerConnection (KVServer).
	 * 
	 * @param connection: The server we send an ECSMessage to
	 * @param encapsulated_message: The message we encapsulate in TextMessage
	 * 
	 * @return: The ECS_Status type (if any) of the message response.
	 * 			NOTE: Can be null.
	 */
	public ECS_Status forwarding_message_to_server(ServerConnection connection, ECSMessage encapsulated_message){
		
		ECS_Status response = null;
		try{
			
			// Set input and output to the server connection
			input = connection.input;
			output = connection.output;
			
			// Create encapsulated string:
			Gson gson = new Gson();
			String encapsulated_string = gson.toJson(encapsulated_message);
			
			// Send TextMessage:
			TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,null,true, false); 
			sendMessage(message_to_send);
			
			// Wait for a response for intializing of metadata:
			TextMessage latestMsg = receiveMessage();
			
			// Take encapsulated ECS message and convert into a usable format:
			if(latestMsg != null){
				
				String encapsulated_ECS_message = latestMsg.getMsg();
				gson = new Gson();
				ECSMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ECSMessage.class);
	
				// Set response type:
				response = json_parameters.get_message_type();
				logger.info("Received a response of type \""+ response +"\" from ("+ connection.metadata.hostname +","+ connection.metadata.port +").");
			}
		}
		catch(IOException e){
			logger.error("Failed to send a message of type \""+encapsulated_message.get_message_type()+"\" to ("+ connection.metadata.hostname +","+ connection.metadata.port +").");
		}
		
		return response;
	}
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes(); 
		String string_format = new String(msgBytes);
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		logger.info("Send message:\t '" + string_format + "'");
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Attempts to wait and read a request from a client.
	 * 
	 * @return: TextMessage - formatted message received NULL - indicating a
	 *          closed connection
	 * 
	 * @throws IOException:
	 *             If there is an external failure reading from the socket
	 */
	public TextMessage receiveMessage() throws IOException {

		// Message to return is initially null to indicate failure reading
		// bytes:
		TextMessage msg = null;

		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;

		/* read first char from stream */
		boolean reading = true;

		// While we haven't read the opening header for a message, keep reading
		// before we record anything
		//
		while (!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading) {
			prev = cur_byte;
			cur_byte = (byte) input.read();

			if (cur_byte == -1) {
				reading = false;
				running = false;
			}
		}

		// Error while reading, ie server close:
		if (!running)
			return msg;

		/* read next char from stream */
		cur_byte = (byte) input.read();

		// While we haven't read the closing header for the message, record
		// everything, this is the JSON object
		//
		boolean stuff_last_char = false;

		while (!(cur_byte == HEADER_CLOSE && prev != MESSAGE_STUFFING) && reading) {

			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if (stuff_last_char) {

				// This was not an attempt to stuff a header open either:
				//
				if (cur_byte != HEADER_OPEN) {
					bufferBytes[index] = cur_byte;
					prev = cur_byte;

					index++;
				}
			}

			// Update whether the current character is a stuff character
			if (cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

			if (!stuff_last_char) {

				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if (index == BUFFER_SIZE) {
					if (msgBytes == null) {
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, BUFFER_SIZE);
					}

					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				}

				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;
				index++;

				/* stop reading is DROP_SIZE is reached */
				if (msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}

			}

			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();

			if (cur_byte == -1) {
				reading = false;
				running = false;
			}

		}

		// Error reading bytes, ie server close:
		if (!running)
			return msg;

		if (msgBytes == null) {
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}

		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for
		// convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);

		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);

		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(),
				json_parameters.get_status(), true, false);

		return msg;

	}

}
