package app_kvEcs;

import com.jcraft.jsch.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;


/*
 * Used for SSH calls
 * 
 * Most basic class available from JSCH examples.
 * 
 */
public class SUserInfo implements UserInfo{
	
    private String password;
    private String passPhrase;
    JTextField passwordField=(JTextField)new JPasswordField(20);

    
    public SUserInfo(String password, String passPhrase) {
        this.password = password;
        this.passPhrase = passPhrase;
    }

    @Override
    public String getPassphrase() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean promptPassphrase(String arg0) {
        return true;
    }

    @Override
    public boolean promptPassword(String message) {
	  return true;
    }

    @Override
    public boolean promptYesNo(String arg0) {
        return true;
    }

    @Override
    public void showMessage(String arg0) {
        System.out.println("SUserInfo.showMessage()");
        /*public void showMessage(String message){
	  	JOptionPane.showMessageDialog(null, message);
		}*/
    }
}


