package app_kvServer;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import org.apache.log4j.*;
import java.net.UnknownHostException;	

import common.messages.MetaMessage;
import common.messages.TextMessage.MessageParameters;
import common.messages.ECSMessage;
import common.messages.TextMessage;
import common.messages.ECSMessage.ECS_Status;
import common.messages.KVMessage.StatusType;
import com.google.gson.*;
import java.util.LinkedHashMap;
import java.util.concurrent.locks.ReadWriteLock;


import java.util.List;
import java.util.ArrayList;
import common.messages.MetadataEntry;





/**
 * Represents a connection end point for a particular client that is 
 * connected to the server. This class is responsible for message reception 
 * and sending. 
 * The class also implements the echo functionality. Thus whenever a message 
 * is received it is going to be echoed back to the client.
 */
public class ClientConnection implements Runnable {

	// In memory cache:
	private volatile Cache data_cache;

	// Server that created this thread:
	KVServer server;
	
	
	/*
	 * Parameters specific to sending/receiving messages:
	 */
	private static Logger logger = Logger.getRootLogger();
	private boolean isOpen;
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 128 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;
	private Socket clientSocket;
	private InputStream input;
	private OutputStream output;
	
	
	/*
	 * Parameters specific to reading from persistent storage:
	 */
	private static final String inner_delimiter = " ";
	private static final String outter_delimiter = "\n";
	private ReadWriteLock storage_lock;
	
	

		
	/**
	 * Constructs a new CientConnection object for a given TCP socket.
	 * @param clientSocket the Socket object for the client connection.
	 */
	public ClientConnection(Socket clientSocket,  Cache dataCache, ReadWriteLock storage_lock, KVServer _server) {
		this.clientSocket = clientSocket;
		this.isOpen = true;
		this.data_cache = dataCache;
		this.storage_lock = storage_lock;
		this.server = _server;
	}
	
	/**
	 * Initializes and starts the client connection. 
	 * Loops until the connection is closed or aborted by the client.
	 */
	public void run() {
		try {
			output = clientSocket.getOutputStream();
			input = clientSocket.getInputStream();

			sendMessage(new TextMessage(
					"Connection to MSRG Echo server established: " 
					+ clientSocket.getLocalAddress() + " / "
					+ clientSocket.getLocalPort(), null, null, null, false, false));

			while(isOpen) {
				try {
					
					
					// Wait for messages after the connection has been established:
					//
					TextMessage latestMsg = receiveMessage();
					if(latestMsg != null && isOpen){
						if(latestMsg.getisECS() != null && latestMsg.getisECS())
							handle_ECS_message(latestMsg);
						else if(latestMsg.get_isServer() != null && latestMsg.get_isServer())		
							handle_server_message(latestMsg);
						else
							handle_message(latestMsg);
					}
					
				/* connection either terminated by the client or lost due to 
				 * network problems*/	
				} catch (IOException ioe) {
					logger.error("Error! Connection lost!");
					isOpen = false;
				}				
			}
			
		} catch (IOException ioe) {
			logger.error("Error! Connection could not be established!", ioe);
			
		} finally {
			
			try {
				if (clientSocket != null) {
					input.close();
					output.close();
					clientSocket.close();
				}
			} catch (IOException ioe) {
				logger.error("Error! Unable to tear down connection!", ioe);
			}
		}
	}
	
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes();
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		String string_format = new String(msgBytes);
		logger.trace("SEND <" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">:" 
				+ string_format);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Handles messages that specifically come from the ECS
	 * 
	 * 
	 * @param message: A TextMessage with "isECS" set as true
	 */
	private void handle_ECS_message(TextMessage message){
		
		// Take encapsulated ECS message and convert into a usable format:
		String encapsulated_ECS_message = message.getMsg();
		Gson gson = new Gson();
		ECSMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ECSMessage.class);
		
		
		
		
		
		// If ECS is requesting us to shutdown:
		if(json_parameters.get_message_type() == ECS_Status.SHUTDOWN){
			
			// Get write lock so that no other threads are writing or reading:
			this.storage_lock.writeLock().lock();
			

			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage send_message = new ECSMessage(ECS_Status.SHUTDOWN_ACK, null, null, null, null, null, -1);
			gson = new Gson();
			String json = gson.toJson(send_message);
			
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true, false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send a shutdown acknowledge to the ECS server.");
			}

			
			// Request server close:
			server.stopServer();
		}
			
		
		
		
		// IF the ECS is requesting us to shutdown hard, also delete all persistent data:
		//
		else if(json_parameters.get_message_type() == ECS_Status.SHUTDOWN_HARD){

			
			// Stop handling all requests:
			server.stop_service();

			// Get write lock so that no other threads are writing or reading:
			this.storage_lock.writeLock().lock();

			// Delete all persistent data:
			remove_pairs_out_of_range(true);
			
			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage send_message = new ECSMessage(ECS_Status.SHUTDOWN_ACK, null, null, null, null, null, -1);
			gson = new Gson();
			String json = gson.toJson(send_message);
			
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true, false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send a shutdown acknowledge to the ECS server.");
			}

			
			// Request server close:
			server.stopServer();

		}
		
		
		// IF the ECS is requesting us to shutdown soft, don't delete any entries
		//
		else if(json_parameters.get_message_type() == ECS_Status.SHUTDOWN_SOFT){
			
			// Stop handling all requests:
			server.stop_service();
			
			// Get write lock so that no other threads are writing or reading:
			this.storage_lock.writeLock().lock();
			
			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage send_message = new ECSMessage(ECS_Status.SHUTDOWN_ACK, null, null, null, null, null, -1);
			gson = new Gson();
			String json = gson.toJson(send_message);
			
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true, false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send a shutdown acknowledge to the ECS server.");
			}

			
			// Request server close:
			server.stopServer();
		}
		
		
		
		
		// If ECS wishes to initialize our metadata:
		else if(json_parameters.get_message_type() == ECS_Status.INITIALIZE_METADATA){
			
			// Update the server's metadata:
			server.initialize_server_metadata(json_parameters.get_metadata());
			server.set_hostname(json_parameters.get_hostname());
			
			
			logger.info("Successfully initialized server metadata.");
			int entry_number = 1;
			for(MetadataEntry metadata: json_parameters.get_metadata()){
				logger.info("Server "+entry_number+ ": (" + metadata.hostname + "," + metadata.port +  ")" + " :  Hash-start:\" "+ metadata.prev_hash_range +"\" Hash-end:\" "+metadata.hash+"\".");
				entry_number++;
			}
			
			
			
			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage send_message = new ECSMessage(ECS_Status.INITIALIZE_ACK, null, null, null, null, null, -1);
			gson = new Gson();
			String json = gson.toJson(send_message);
			
			
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true,false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send a initialize metadata acknowledge to the ECS server.");
			}

		}
		
		
		
		
		
		
		
		
		
		// If ECS wishes to start or stop the storage service of this server:
		else if(json_parameters.get_message_type() == ECS_Status.START_SERVICE || json_parameters.get_message_type() == ECS_Status.STOP_SERVICE){
			
			// Update the server's service state:
			if(json_parameters.get_message_type() == ECS_Status.START_SERVICE){
				server.start_service();
				logger.info("Received request from ECS to start storage service.");
			}
			else{
				server.stop_service();
				logger.info("Received request from ECS to stop storage service.");
			}

			
			
			// Create an ECS message ---> turn it in a string to pass
			// through a "TextMessage"
			//
			ECSMessage send_message = null;
			if(json_parameters.get_message_type() == ECS_Status.START_SERVICE)
				send_message = new ECSMessage(ECS_Status.START_SERVICE_ACK, null, null, null, null, null, -1);
			else
				send_message = new ECSMessage(ECS_Status.STOP_SERVICE_ACK, null, null, null, null, null, -1);
			
			gson = new Gson();
			String json = gson.toJson(send_message);
			
			
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true,false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send service state update acknowledge to the ECS server.");
			}

		}
		
		//ECS wants to lock write functions of the server
		else if (json_parameters.get_message_type() == ECS_Status.WRITE_LOCK){
			
			//lock the write capability of the server
			server.is_write_locked = true;
			
			
			ECSMessage send_message = new ECSMessage(ECS_Status.WRITE_LOCK_ACK, null, null, null, null, null, -1);

			gson = new Gson();
			String json = gson.toJson(send_message);
			
			// Send an ACK:
			TextMessage message_to_send = new TextMessage(json, null,null,null,true,false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send service state update acknowledge to the ECS server.");
			}
			
		}


		
		//unlock the write functionality of the server
		else if (json_parameters.get_message_type() == ECS_Status.WRITE_UNLOCK){
			
			// If we are unlocking, we must have move data to another server.
			// Clean up old KV pairs
			remove_pairs_out_of_range(false);
			
			
			// Lock the write capability of the server
			server.is_write_locked = false;
			
			// Send an ACK:
			ECSMessage send_message = new ECSMessage(ECS_Status.WRITE_UNLOCK_ACK, null, null, null, null, null, -1);

			gson = new Gson();
			String json = gson.toJson(send_message);
			TextMessage message_to_send = new TextMessage(json, null, null, null, true, false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send service state update acknowledge to the ECS server.");
			}
			
		}
		
		
		
		
		
		
		// ECS wants to migrate a subset of our data to another server
		else if (json_parameters.get_message_type() == ECS_Status.MOVE_DATA){
			
			String destination = json_parameters.get_move_destination();
			byte[] start_range = json_parameters.get_start_range();
			byte[] end_range = json_parameters.get_end_range();
			int port = json_parameters.get_port();
			
			// Attempt to move data from server to server:
			Boolean success = move_server_data(destination, port, start_range, end_range);
			
			
			
			
			// Give appropriate ACK to the ECS:
			ECSMessage send_message = null;
			if(success)
				send_message = new ECSMessage(ECS_Status.MOVE_DATA_SUCCESS, null, null, null, null, null, -1);
			else
				send_message = new ECSMessage(ECS_Status.MOVE_DATA_FAILURE, null, null, null, null, null, -1);

			
			
			// Encapsulate ECS message into TextMessage:
			gson = new Gson();
			String json = gson.toJson(send_message);			
			TextMessage message_to_send = new TextMessage(json, null, null, null, true, false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException e){
				logger.error("Failed to send moveData acknowledge to the ECS server.");
			}
		}
		
		
	}
	
	



	
	/**
	 * 
	 * Helper function for handle_ECS_message():
	 * 
	 * Assumes metadata is up to date if you have a conditional delete. This uses current metadata to remove any key-value pairs that no longer belong in the server's current hash range:
	 * 
	 * @param delete_all: True --> deletes a KV pair unconditionally
	 * 
	 */
	void remove_pairs_out_of_range(Boolean delete_all){
		
		// Acquire read lock in order to read all key value pairs:
		this.storage_lock.readLock().lock();
		List<String> old_keys = new ArrayList<String>();
		
		try{
				
			// Open the persistent storage file:
			Path file = Paths.get(KVServer.storage_filename);
			try (
				
				// Create input stream:
				InputStream in = Files.newInputStream(file);
					
			    BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
			    String line = null;
			    
			    
			    // For each line in the persistent storage:
			    //
			    while ((line = reader.readLine()) != null) {
			        
			    	String[] key_value_splitted = line.split(" ");
			    	String key = key_value_splitted[0];
			    	
			    	// Check if key is NOT in range:
			    	if(delete_all || (!server.is_key_in_range(key)))
			    		old_keys.add(key); 			// Add key to old key list
	
			    }
			} catch (IOException x) {
			    logger.error(x);
			}
			
			
		// Done reading persistent storage:
		}finally{
			this.storage_lock.readLock().unlock();
		}
	
		// Remove every old key:
		for(String old_key : old_keys){
			set_storage(old_key, "null");
		}			
	}
	
	
	
	
	
	
	/**
	 * 
	 * (Helper function for handle_ECS_message)
	 * Attempts to connect to a destination host and send all relevant messages in the persistent storage that need to be moved.
	 * 
	 * @param destination_host: The host we are moving data to
	 * @param port: The port of the destination host
	 * @param start_range: The start of the hash range we are moving
	 * @param end_range: The end of the hash range we are moving 
	 * 
	 * @return: Success of moving the data (were we able to connect and send messages)
	 * 
	 */
	boolean move_server_data(String destination_host, int port, byte[] start_range, byte[] end_range){
		
		// Did we successfully connect to the desintation server:
		Boolean successful_connect = false;
		
		// Temporarily hold the input and output streams to the ECS while we esablish communication with the other server:
		InputStream temp_input = input;
		OutputStream temp_output = output; 
		Socket new_connection = null;
		
		
		
		
		
		
		// Try to get the connect response from the newly launched KVServer:
		try {
			
			// Attempt to connect:
			new_connection = new Socket(destination_host, port);
			
			
			output = new_connection.getOutputStream();
			input = new_connection.getInputStream();
			isOpen = true;
			TextMessage connect_response = receiveMessage();

				
			
			
			// On connection success:
			if(connect_response != null){
				successful_connect = true;
			}			
		
			// Connection closes when attempting to read a message:
			else{
				logger.error("Connection closed during connect phase to the following server:\n");
				logger.error("Hostname: \"" + destination_host + "\"\n");
				logger.error("Port: \"" + port + "\"\n");
			}
				
		} 
			
		// No such host exists:
		catch (UnknownHostException e){
			logger.error("Hostname was not found for the following server:\n");
			logger.error("Hostname: \"" + destination_host + "\"\n");
			logger.error("Port: \"" + port + "\"\n");
		}
		// Trouble opening a socket:
		catch (IOException ioe) {
			logger.error("Input and output streams could not be established for the following server:");
			logger.error("Hostname: \"" + destination_host + "\"\n");
			logger.error("Port: \"" + port + "\"\n");
		}
			
		
		
	
	
		
		/*
		 * 
		 * If we successfully connected to the server, proceed to send messages of key value pairs being moved:
		 * 
		 */
		Boolean successful_transfer = successful_connect; // If we even connected successfully
		if (successful_connect) {
			
			
			// Acquire read lock in order to read all key value pairs:
			this.storage_lock.readLock().lock();
			
			try{
					
				// Open the persistent storage file:
				Path file = Paths.get(KVServer.storage_filename);
				try (
					
					// Create input stream:
					InputStream in = Files.newInputStream(file);
						
				    BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
				    String line = null;
				    
				    
				    
				    
				    // For each line in the persistent storage:
				    // While we haven't failed to send a key value pair (ie closed connection)
				    //
				    while ((line = reader.readLine()) != null  && successful_transfer) {
				        
				    	String[] key_value_splitted = line.split(" ");
				    	String key = key_value_splitted[0];
				    	
				    	// Check if key is in move range:
				    	if(server.is_in_move_range(key, start_range, end_range)){
				    		
				    		// Concat the value if necessary:
				    		String concat_value = key_value_splitted[1];
				    		for(int i=2; i<key_value_splitted.length; i++){
				    			concat_value = concat_value + " " + key_value_splitted[i];
				    		}
				    		
				    		
				    		// Send a server specific message:
							TextMessage message_to_send = new TextMessage(null, key, concat_value, null,false,true); 
							try{
								sendMessage(message_to_send);
							}
							catch(IOException e){
								successful_transfer = false;
								logger.error("Failed to send key value pair to the server.");
							}
				    		
				    	}
		
				    }
				} catch (IOException x) {
				    logger.error(x);
				}
				
				
			// Done reading persistent storage:
			}finally{
				this.storage_lock.readLock().unlock();
			}
			
			
			
			
			
			/*
			 * Send a closing message and wait for an "ACK"
			 */
			
			// Send a server specific message to indicate closing:
			TextMessage message_to_send = new TextMessage(null, "null", "null", null,false,true); 
			try{
				sendMessage(message_to_send);
				TextMessage end_move_data_response = receiveMessage();
				
				// Wait for an ACK of the finishing of writing results:
				if(end_move_data_response != null && end_move_data_response.getKey().equals("null") && end_move_data_response.getValue().equals("null")){
					successful_transfer = true;
				}			
			
				// Connection closes when attempting to get an ACK:
				else{
					successful_transfer = false;
					logger.error("Connection closed during move data phase phase to the following server (Didn't receive closing ACK):\n");
					logger.error("Hostname: \"" + destination_host + "\"\n");
					logger.error("Port: \"" + port + "\"\n");
				}
					
			}
			catch(IOException e){
				successful_transfer = false;
				logger.error("Failed to send key value pair to the server.");
			}
			
			
			
			
			
			
			// Close server connection;
			try{
				new_connection.close();			
			}
			catch(IOException e){
				logger.error("Failed to close socket for server to server connection.");
			}
		}
		
		
		
		
		
		// Reset all values after having finished communicating with the server:
		isOpen = true;
		output = temp_output;
		input = temp_input;
		
		return successful_transfer;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/**
	 * Handles messages that specifically come from another server, currently intended for data migration.
	 * 
	 * @param message: A TextMessage with "isServer" set as true
	 */
	private void handle_server_message(TextMessage message){
		
		// Store this key-value pair received:
		String key = message.getKey();
		String value = message.getValue();
		
		// Indicates its the end of move data --> send back an ACK:	
		//
		if(key.equals("null") && value.equals("null")){
			TextMessage message_to_send = new TextMessage(null, "null","null",null,false,false); 
			try{
				sendMessage(message_to_send);
			}
			catch(IOException ioe){
				logger.fatal(ioe);
			}
		}
			
		
		// Just another KV pair to store:
		//
		else{
			// Place in cache:
			data_cache.put(key, value);
	
			// Write to storage:
			if(set_storage(key, value) < 0){
				logger.error("unable to write to storage");
			}
			logger.trace("Inserted KV-pair <" + message.getKey() + ", "+ message.getValue() + ">.");
		}
		
	}







	/**
	 *
	 * Send a object encapsulated in a TextMessage to the client
	 * (Note: The client is able to discern the object exists through the status type.)
	 *
	 */
	public void forword_message_to_client(Object object, StatusType status_send){
		
		try{
			
			// Create encapsulated string:
			Gson gson = new Gson();
			String encapsulated_string = gson.toJson(object);
			
			
			// Send TextMessage:
			TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,status_send,false,false); 
			sendMessage(message_to_send);
			
		}
		catch(IOException e){
			logger.error("Failed to send metadata message from server to client");
		}
		
		
	}	
	
	
	
	
	
	/**
	 * 
	 * Takes a message received (a request from a client) and generates the appropriate response to send back.
	 * 
	 * NOTE: This function specifically handles client messages, ECS messages are handled by another function.
	 * 
	 * 
	 * @param message: message received by the server
	 */
	void handle_message(TextMessage message){
		
		// Our response:
		TextMessage response = null;

		
		/*
		 * 
		 * First determine whether or not we want to process this request:
		 * 
		 * 
		 */
		
		// First check if the storage service for this server is in fact on:
		if(!server.get_service_status()){
			
			// Server metadata sent with message:
			MetaMessage msg_to_send = new MetaMessage(server.metadata_entries);	
			forword_message_to_client(msg_to_send, StatusType.SERVER_STOPPED);	
		}
		else{
			
			
			/*
			 * 
			 *  Check if our server is responsible for this hash range:
			 *  
			 */
			if( !server.is_key_in_range(message.getKey()) ){
			
				// Server metadata sent with message:
				MetaMessage msg_to_send = new MetaMessage(server.metadata_entries);	
				forword_message_to_client(msg_to_send, StatusType.SERVER_NOT_RESPONSIBLE);	
			
			}
			
			
			
			
			
			else
			{
			
				/*
				 * 
				 * IF we are here, we are ready to attempt to process the request:
				 * 
				 * 
				 */
				
				// Check if we have a response for the given message type:
		
				
				if(message.getStatus() == StatusType.GET){
		
					String value_from_cache = data_cache.get(message.getKey());
					String value_from_storage = get_storage(message.getKey());
		
		
					// If it actually exists:
					if(value_from_cache != null){//can find in cache, TO TERRY: need to implement the cache searching
		
						response = new TextMessage(null,message.getKey(),value_from_cache,StatusType.GET_SUCCESS, false, false);
						logger.trace("Retrieved value: <" + message.getKey() + "," + value_from_cache + "> from cache");
					}
					else if(value_from_storage!=null){//cannot find in cache but can find in storage
		
						response = new TextMessage(null,message.getKey(),value_from_storage,StatusType.GET_SUCCESS, false, false);
						data_cache.put(message.getKey(), value_from_storage);
						logger.trace("Retrieved value: <" + message.getKey() + "," + value_from_storage + "> from storage");
		
					}else{//cannot find anywhere
						response = new TextMessage(null,message.getKey(),null,StatusType.GET_ERROR, false, false);
						logger.trace("The Key <" + message.getKey() + "> does not exist.");
		
					}
				}
				
				
				
				else if(message.getStatus() == StatusType.PUT){
					
					//if write lock is in effect, shouldn't allow the client to write anything
					if(server.is_write_locked){
						
						forword_message_to_client(server.metadata_entries, StatusType.SERVER_WRITE_LOCK);
						
					}
					
					//!equals("null"), valid put value
					else if(!message.getValue().equals("null")){//insert new key-value pair
					
						String value_from_cache = data_cache.put(message.getKey(), message.getValue());
						String value_from_storage = get_storage(message.getKey());
						
						int return_from_storage;

						
						//write to persistent storage
						if((return_from_storage=set_storage(message.getKey(), message.getValue())) < 0){
							logger.error("unable to write to storage");
						}
		
						//respond to client
		
						if(value_from_cache.equals(message.getValue()) || value_from_storage.equals(message.getValue()))//
							response = new TextMessage(null,message.getKey(), message.getValue(),StatusType.PUT_SUCCESS, false, false); 
						else
							response = new TextMessage(null,message.getKey(),message.getValue(),StatusType.PUT_UPDATE, false, false);
		
						logger.trace("Inserted KV-pair <" + message.getKey() + ", "+ message.getValue() + ">.");
					}
					
					// Remove it (indicated by the null for <value>):
					//
					else{//remove k-v pair
						
						String value_from_cache = data_cache.get(message.getKey());
						String value_from_storage = get_storage(message.getKey());
		
					if(value_from_cache!= null || value_from_storage!=null){
		
							data_cache.remove(message.getKey());					
							//remove it from the storage
							if(set_storage(message.getKey(), message.getValue()) < 0){
								System.err.println("unable to delete from storage");
							}
							
							response = new TextMessage(null,message.getKey(),message.getValue(),StatusType.DELETE_SUCCESS, false, false);
							logger.trace("Deleted KV-pair with key <" + message.getKey() + ">.");
						}
						
						// Failure: don't write to the storage
						else{
							response = new TextMessage(null,message.getKey(),message.getValue(),StatusType.DELETE_ERROR, false, false);
							logger.trace("The KV-pair with key <" + message.getKey() + "> does not exist");
						}
					}
				}
			
			}
		
		}
		
		
		
		
		// If we have a response to send back, send it
		if(response != null){
			try{
				sendMessage(response);
			}
			catch (IOException ioe){
				logger.info("Failed to send response to client!");
			}
		}
	}
	
	
	
	
	
	
	//server storage helper functions
	//reference: http://stackoverflow.com/questions/7888004/how-do-i-print-escape-characters-in-java
	public static String unEscapeString(String s){
		
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<s.length(); i++)
			switch (s.charAt(i)){
				case '\n': sb.append("\\n"); break;
				case '\t': sb.append("\\t"); break;
				// ... rest of escape characters
				default: sb.append(s.charAt(i));
			}
		return sb.toString();
	
	}	
	
	
	
	/**
	* This method gets the value based on the key user gives from the persistent storage
	* It should only be called by internal functions in clientconncection.java
	* @param  key  the key used to get key-value pair from the storage
    * @return      value got from the storage based on the key given
	*/
	private String get_storage (String key) {
		
		this.storage_lock.readLock().lock();//lock
		
		try{
				
			Path file = Paths.get(KVServer.storage_filename);
			try (InputStream in = Files.newInputStream(file);
			    BufferedReader reader =
			      new BufferedReader(new InputStreamReader(in))) {
			    String line = null;
			    while ((line = reader.readLine()) != null) {
			        
			    	String[] key_value_splitted = line.split(" ");
			    	if(key_value_splitted[0].equals(key)){//return the value
			    		//return key_value_splitted[1];
			    		
			    		String ret_value_string = key_value_splitted[1];
			    		
			    		for(int i=2; i<key_value_splitted.length; i++){
			    			
			    			ret_value_string = ret_value_string + " " + key_value_splitted[i];
			    			
			    		}
			    		
			    		return ret_value_string;
			    		
			    	}
	
			    }
			} catch (IOException x) {
			    System.err.println(x);
			}
			
			
		}finally{//unlock
		
			this.storage_lock.readLock().unlock();
		
		}
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * This method can:
	 * 1. insert a key-value pair to the storage
	 * 2. delete a key-value pair form the storage (if value=="null")
	 * 3. update a key-value pair in the storage
	 * @param  key  the key used to set key-value pair in the storage
	 * @param  value the value used to set key-value pair in the storage. (It is a delete operation if value == "null")
	 * @return		0 if the set operation is successful, -1 if it's unsuccessful
	 */
	private int set_storage(String key, String value){
		
		this.storage_lock.writeLock().lock();//lock
		
		try{
		
			if(key == null){//this shouldn't happen
				return -1;
			}
			
			//Path file = Paths.get(KVServer.storage_filename);
	
			//System.out.println("<" + value + ">");
	
			boolean is_remove = value.equals("null");
			
			//if(value.equals("null")){//remove key from the storage
				
				File inputFile = new File(KVServer.storage_filename);
				File tempFile = new File("./myTempFile.txt");
	
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new FileReader(inputFile));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				BufferedWriter writer = null;
				try {
					writer = new BufferedWriter(new FileWriter(tempFile));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				if(is_remove){//remove k-v from storage
	
					//String lineToRemove = "<" + key + ">" + "," + "<" + value + ">";
					String lineToRemove = key;
					String currentLine;
	
					//System.out.println("line to remove is <" + lineToRemove + ">");
					try {
	
						while((currentLine = reader.readLine()) != null) {
	
						    // trim newline when comparing with lineToRemove
						    String trimmedLine = currentLine.trim();
						    String[] key_value_splitted = trimmedLine.split(" ");
	
						    //System.out.println("current line is: <" + trimmedLine + ">");
	
						    if(key_value_splitted[0].equals(lineToRemove)){ 
						   		//System.out.println("skip the soon to be deleted line");
						    	continue;
						    }
			
						    writer.write(currentLine + System.getProperty("line.separator"));
						}
	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	
				}else{//insert k-v to storage
	
					//System.out.println("JM DEBUG: ABOUT TO INSERT A KV-pair");
	
					boolean is_replaced = false;
	
					String currentLine;
					try {
	
						while((currentLine = reader.readLine()) != null) {
	
						    // trim newline when comparing with lineToRemove
						    String trimmedLine = currentLine.trim();
						    String[] key_value_splitted = trimmedLine.split(" ");
	
						    if(key_value_splitted[0].equals(key)){//duplication: the input k-v pair already exists. Then just write new value instead of old value
						    	is_replaced = true;
						   		currentLine = key + inner_delimiter + value;
						    }
							currentLine = unEscapeString(currentLine);
						    writer.write(currentLine + System.getProperty("line.separator"));
						}
	
						if(is_replaced==false){//if there was no duplication replacement, then write a new line at the end of the file
	
							currentLine = key + inner_delimiter + value;
							currentLine = unEscapeString(currentLine);
							writer.write(currentLine);
	
						}
	
	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	
	
				}
	
	
	
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				boolean successful = tempFile.renameTo(inputFile);
				
				if(successful){
					return 0;
				}else{
					return -1;
				}
	
				
				
		}finally{
			
			this.storage_lock.writeLock().unlock();//unlock
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	

	/**
	 * Attempts to wait and read a request from a client.
	 * 
	 * @return: TextMessage - formatted message received 
	 * 			NULL - indicating a closed connection
	 * 
	 * @throws IOException: If there is an external failure reading from the socket
	 */
	private TextMessage receiveMessage() throws IOException {
		
		// Message to return is initially null to indicate failure reading bytes:
		TextMessage msg = null;
		
		
		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;
		
		/* read first char from stream */
		boolean reading = true;


		// While we haven't read the opening header for a message, keep reading before we record anything
		//
		while(!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading){
			prev = cur_byte;
			cur_byte = (byte)input.read();
			

			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
		}
		
		// Error while reading, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		/* read next char from stream */
		cur_byte = (byte) input.read();
		
				
		
		
		// While we haven't read the closing header for the message, record everything, this is the JSON object
		//
		boolean stuff_last_char = false;
		
		while(!(cur_byte == HEADER_CLOSE &&  prev!= MESSAGE_STUFFING) && reading) {
			
			
			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if(stuff_last_char){

				// This was not an attempt to stuff a header open either:
				//
				if(cur_byte != HEADER_OPEN){
					bufferBytes[index] = cur_byte;
					prev = cur_byte;
							
					index++;
				}
			}
			
			
			
			// Update whether the current character is a stuff character
			if(cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

				
			
			
			if(!stuff_last_char){
					
				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if(index == BUFFER_SIZE) {
					if(msgBytes == null){
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length,
								BUFFER_SIZE);
					}
	
					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				} 
				
				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;						
				index++;
				
				/* stop reading is DROP_SIZE is reached */
				if(msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}
			
			}
			
			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();
			
			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
				
		}
		
		// Error reading bytes, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		if(msgBytes == null){
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}
		
		
		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);
		
		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);
		
		
		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(), json_parameters.get_status(), json_parameters.get_isECS(), json_parameters.get_isServer());
		
		logger.info("RECEIVE \t<" 
				+ clientSocket.getInetAddress().getHostAddress() + ":" 
				+ clientSocket.getPort() + ">: '" 
				+ string_format + "'");
				
		return msg;		

    }
	

	
	
	
	
	
	
}

