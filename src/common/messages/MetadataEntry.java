package common.messages;


/*
 *  Keeps track of a single server entry in a consistent hashing ring:
 */
public class MetadataEntry{
	public String hostname;
	public int port;
	public byte[] hash = null; 				// Hash of hostname and port 
	public byte[] prev_hash_range = null; 	// ie. Previous hash in the ring
	
	public MetadataEntry(String _hostname, int _port){
		this.hostname =_hostname;
		this.port = _port;
	}
}