package common.messages;
import com.google.gson.*;


/**
 * Represents a simple text message, which is intended to be received and sent 
 * by the server.
 * 
 * 
 * FORMAT OF THE MESSAGE:  HEADER_OPEN<json_structure_here>HEADER_CLOSE
 * 
 *  The final message in byte form is placed in msgBytes.
 *  The <json_structure_here> contains everything in message_parameters.
 * 
 */
public class TextMessage implements KVMessage {

	private byte[] msgBytes; // Entire message in byte form - ready to send
	public final byte HEADER_OPEN = 0x7E; // "~"
	public final byte HEADER_CLOSE = 0x5E; // "^"
	public final byte MESSAGE_STUFFING = 0x23; // "#"
										// Used to indicate that a "header_open" or "header_close" in a message 
									   // is not a genuine header open or close.
	
	
	
	// This will be turned into JSON
	// Then put into msgBytes
	//
	public class MessageParameters{
		private String msg; // Actual message content
		private String key;	// Key associated with message...
		private String value; 
		private StatusType status;
		private Boolean isECS;
		private Boolean isServer; 

		public String get_msg(){
			return msg;
		}
		public String get_key(){
			return key;
		}
		public String get_value(){
			return value;
		}
		public StatusType get_status(){
			return status;
		}
		public Boolean get_isECS(){
			return isECS;
		}
		public Boolean get_isServer(){
			return isServer;
		}
	}
	private MessageParameters pre_json;
	
	
	
	
	
    /**
     * 
     * Constructs a TextMessage object, used to send messages via msgBytes
     * 
     * @param msg: Message to send, actual text content
     * @param key: Key if associated, defaults to null
     * @param value: Value of the pair if associated with the message, defaults to null
     * @param status: What type of message this is
     */

	public TextMessage(String msg, String key, String value, StatusType status, Boolean _isECS, Boolean _isServer) {
		this.pre_json = new MessageParameters();
		this.pre_json.key = key;
		this.pre_json.value = value;
		this.pre_json.msg = msg;
		this.pre_json.status = status;
		this.pre_json.isECS = _isECS;
		this.pre_json.isServer = _isServer;
		this.msgBytes = string_to_message_bytes(this.pre_json);
	}
	
	
	
	
	
	
	
	
	
	/* 
	 * 
	 * INTERFACE FUNCTIONS
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	/**
	 * @return the key that is associated with this message, 
	 * 		null if not key is associated.
	 */
	public String getKey(){
		return this.pre_json.key;
	}
	
	/**
	 * @return the value that is associated with this message, 
	 * 		null if not value is associated.
	 */
	public String getValue(){
		return this.pre_json.value;
	}
	
	/**
	 * @return a status string that is used to identify request types, 
	 * response types and error types associated to the message.
	 */
	public StatusType getStatus(){
		return this.pre_json.status;
	}
	public Boolean getisECS(){
		return this.pre_json.isECS;
	}
	public Boolean get_isServer(){
		return this.pre_json.isServer;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/* 
	 * 
	 * HELPER FUNCTIONS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * Returns the content of this TextMessage as a String.
	 * 
	 * @return the content of this message in String format.
	 */
	public String getMsg() {
		
		if(this.pre_json.msg != null)
			return this.pre_json.msg.trim();
		else 
			return null;
	}

	
	/**
	 * Returns an array of bytes that represent the ASCII coded message content.
	 * 
	 * @return the content of this message as an array of bytes 
	 * 		in ASCII coding.
	 */
	public byte[] getMsgBytes() {
		return this.msgBytes;
	}
	

	/**
	 * 
	 * @param pre_json Message parameters turned into json and into the final message format
	 * 
	 * @return the final messages format ready in bytes[]
	 */
	private byte[] string_to_message_bytes(MessageParameters pre_json){
		
		// Convert pre_json --> json
		// Compose header open, json and heade close
		//
		Gson gson = new Gson();
		String json = gson.toJson(pre_json);
		int number_of_opens = json.length() - json.replace("~", "").length(); // Counts number of "~" in string
		int number_of_closes = json.length() - json.replace("^", "").length();
		int number_of_stuffs = number_of_opens + number_of_closes;
		
		
		// Prepare for creating the actual message:
		byte[] header_open = new byte[]{HEADER_OPEN};
		byte[] header_close = new byte[]{HEADER_CLOSE};
		byte[] json_byte_form =  new byte[json.length() + number_of_stuffs];
		byte[] final_bytes = new byte[json_byte_form.length + header_open.length + header_close.length + number_of_stuffs];
		System.arraycopy(header_open, 0, final_bytes, 0, header_open.length);
		
		
		
		
		// Enter the actual JSON + stuffing:
		//
		int json_index = 0, cur_index = 0;
		char temp_stuffing = (char)MESSAGE_STUFFING;

		for(cur_index = 0; cur_index < final_bytes.length && json_index < json.length(); cur_index++){
			

			// If we need to add stuffing for a special character, add it:
			//
			if(json.charAt(json_index) == (char)HEADER_OPEN || json.charAt(json_index) == (char)HEADER_CLOSE){
				json_byte_form[cur_index+1] = (byte)json.charAt(json_index);
				json_byte_form[cur_index] = (byte)temp_stuffing;
				cur_index++;
			}
			else
				json_byte_form[cur_index] = (byte)json.charAt(json_index);
			
			json_index++;
		}
		
		System.arraycopy(json_byte_form, 0, final_bytes, header_open.length, json_byte_form.length);
		System.arraycopy(header_close, 0, final_bytes, header_open.length + json_byte_form.length, header_close.length);

		
		return final_bytes;		
	}
	
}
