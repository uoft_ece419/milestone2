package common.messages;

import java.util.List;

public class MetaMessage {

	public List<MetadataEntry> metadata_entries;

	public MetaMessage(List<MetadataEntry> _metadata_entries){
		
		this.metadata_entries = _metadata_entries;
			
	}
	
}
