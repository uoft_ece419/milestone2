package testing;

import java.io.*;
import java.io.IOException;
import client.KVStore;

class GetWorker implements Runnable {
	private Thread t;
	private String threadName;
	String [] keys;

	GetWorker(int threadNum) {
		//System.out.println("Creating threads");

		threadName = Integer.toString(threadNum);
	
	}

	public void setKeys(String [] k)
	{
		keys = k;
	}	
	
	public void run() {
		KVStore kvclient = new KVStore("localhost", 40501);

		long avg_time = 0;

		try {
			kvclient.connect();
			
			for(String getKey : keys)
			{
				long startTime = System.nanoTime();
				kvclient.get(getKey);
				long endTime = System.nanoTime();
			
				avg_time += endTime - startTime;
			}

			avg_time = avg_time / (keys.length);

			System.out.println(threadName + ":" + avg_time);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public void start() {
		if(t == null) {
			t = new Thread (this, threadName);
			t.start();
		}
	}

}
public class perf {

	public static void main(String[] args) {

	KVStore kvclient = new KVStore("localhost", 40502);

	int numWorkers = 5;
	int getNum = 20;
	String[][] keys = new String[numWorkers][getNum];

	try {

		kvclient.connect();

		System.out.println("Loading values");

		BufferedReader reader = new BufferedReader(new FileReader("short_input.txt"));

		System.out.println("Opened file");

		String line;
	
		try {
	
		while((line = reader.readLine()) != null)
		{
			String []keyValue = line.split(",");
			
			kvclient.put(keyValue[0], keyValue[1]);	
		}
		} catch (Exception e){
			e.printStackTrace();
		}
		
		System.out.println("Finished loading data");

		//Seperate out the keys for the workers


		reader.close();

		BufferedReader sec_reader = new BufferedReader(new FileReader("short_input.txt"));

		for(int i = 0; i < numWorkers; i++)
		{
			for(int j = 0; j < getNum; j++)
			{
				if((line = sec_reader.readLine()) != null);
				{
				String key[] = line.split(",");
				keys[i][j] = key[0]; 
				}
			}
		}
	
		System.out.println("Finished loading worker keys");

		//Loop through all the connections

		System.out.println("Starting workers");

		for(int counter = 0; counter < numWorkers; counter++) {
			GetWorker worker = new GetWorker(counter);
			worker.setKeys(keys[counter]);
			worker.start();
		}
		
	} catch (Exception e) {

	}	
	}
}


