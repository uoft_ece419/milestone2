package testing;

import org.junit.Test;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSFunc;
import client.KVStore;

import junit.framework.TestCase;
import java.io.IOException;
import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;

//Fixed Method Order is used to ensure continuity of deletes and puts

public class AdditionalTest extends TestCase {
	
	/***
	 *	@config file testECS.config sets up 4 servers 
	 *	
	 *
	 */
	
	//Use the objects for EF and EC to test 
	private ECSClient EClient = AllTests.EC;
	private ECSFunc EFunc = AllTests.EF;

	private KVStore kvc1;
	private KVStore kvc2;
	private KVStore kvc3;
	private KVStore kvc4;
	

	//Make sure the servers have been initialized and started
	public void setUp() {

		try {
			EClient.handle_command("start", EFunc);

			//Test multiple connections
			kvc1 = new KVStore("localhost", 40501);
			kvc2 = new KVStore("localhost", 40502);
			kvc3 = new KVStore("localhost", 40503);
			kvc4 = new KVStore("localhost", 40504);
		

			kvc1.connect();
			kvc2.connect();
			kvc3.connect();
			kvc4.connect();

		} catch (Exception e) {
			
		}
	}
	

	//Connect to all 4 servers to ensure all are running
	public void testStartAllServers() {

		Exception ex = null;

		try {

			EClient.handle_command("start", EFunc);

		} catch (Exception e) {
			ex = e;
		}

		assertNull(ex);
	}

	public void testStopAllServers() {
		Exception ex = null;
		String key = "hello";
		String value = "world";
		KVMessage response = null;
		
		try {

			EClient.handle_command("stop", EFunc);


			response = kvc1.put(key, value);
		} catch (Exception e) {
			ex = e;
		}

		assertTrue(ex == null && (response.getStatus() == StatusType.SERVER_STOPPED));

	}
	
	public void testPutWrongServer() {
		Exception ex = null;
		String key = "foo";
		String value = "bar";

		KVMessage response = null;
		
		try {

			EClient.handle_command("start", EFunc);

			response = kvc1.put(key, value);
		} catch (Exception e) {
			ex = e; 
		}

		assertTrue(ex == null && (response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE));
	}

	public void testAddNode() {
		Exception ex = null;
		
		KVStore kvc5 = new KVStore("localhost", 40505);

		try {
			EClient.handle_command("addNode 1024 FIFO", EFunc);

			kvc5.connect();
		} catch (Exception e) {
			ex = e;
		}

		assertNull(ex);
	}

	
	public void testPutAndGet() {

		Exception ex = null;

		String key = "hello";
		String value = "world";
		KVMessage response = null;
		KVMessage response2 = null;
		

		try {
			kvc1.connect();
			kvc2.connect();

			response = kvc1.put("hello", "world");
			response2 = kvc2.get("hello");
		} catch (Exception e) {
			ex = e; 
		}

		assertTrue(ex == null && response2.getStatus() == StatusType.GET_SUCCESS && response2.getValue().equals(value));
	}

	//Try to get value from incorrect server
	public void testIncorrectServer() {

		Exception ex = null;
		String key = "hello";	
		KVMessage response = null;

		try {
			kvc1.connect();

			response = kvc1.get("hello");
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(ex == null && response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE);
	}
	

	
	@Test
	public void testServerStop() {
		Exception ex = null;

		KVStore kvc = new KVStore("localhost", 40502);
		KVMessage response = null;

		try {
			
			EClient.handle_command("stop", EFunc);
			kvc.connect();

			response = kvc.put("hello", "world");
		} catch (Exception e) {
			ex = e;
		}

		assertTrue(ex == null && response.getStatus() == StatusType.SERVER_STOPPED);
	}


	public void testServerShutdown() {
		Exception ex = null;

		KVStore kvc = new KVStore("localhost", 40501);
		
		try {
			EClient.handle_command("shutdown", EFunc);
			kvc.connect();
			
		} catch (Exception e) {
			ex = e;
		}
		assertTrue(ex != null);
	}

	@Test
	public void testRemoveNode() {
		Exception ex = null;

		KVStore kvc = new KVStore("localhost", 40505);
		
		try {
			EClient.handle_command("initService 1 1024 FIFO", EFunc);
			EClient.handle_command("removeNode", EFunc);


			kvc.connect();
		} catch (Exception e) {
			ex = e;
		}

		assertTrue(ex != null);

	}
	
}
